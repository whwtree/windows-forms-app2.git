﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        public static string UserName = "";
        public static string PassWord = "";

        private void button1_Click(object sender, EventArgs e)
        {
            if (textUserName.Text.Trim() == "")
            {
                MessageBox.Show("请输入用户名！");
                return;
            }
            if (textPassword.Text.Trim() == "")
            {
                MessageBox.Show("请输入密码！");
                return;
            }
            UserName = textUserName.Text.Trim();
            PassWord = textPassword.Text.Trim();
            if (valid(UserName, PassWord))
            {
                // 登录成功，显示主页面
                MainForm mainFrm = new MainForm();       
                mainFrm.Show();
                // 隐藏当前页面
                this.Hide();
            }
            else
            {
                MessageBox.Show("用户名或密码错误！");
                return;

            }

        }

        private Boolean valid(string userName, string password)
        {
            if (userName == "admin" && password == "123456")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
