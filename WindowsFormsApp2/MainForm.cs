﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class MainForm : Form
    {
        private string UserName = "";
        private string password = "";
        public MainForm()
        {
            InitializeComponent();
            UserName = LoginForm.UserName;
            password = LoginForm.PassWord;
            label_Username.Text = "用户名：" + UserName;
            label_Password.Text = "密  码：" + password;
        }
    }
}
